import WordingBoxEditing from './wordingboxediting';
import WordingBoxUI from './wordingboxui';
import Plugin from '@ckeditor/ckeditor5-core/src/plugin';

export default class WordingBox extends Plugin {
    static get requires() {
        return [WordingBoxEditing, WordingBoxUI];
    }
}
