import Command from '@ckeditor/ckeditor5-core/src/command';

export default class InsertWordingBoxCommand extends Command {
    execute() {
        this.editor.model.change(writer => {
            // Insert <wordingbox>*</wordingbox> at the current selection position
            // in a way that will result in creating a valid model structure.
            this.editor.model.insertContent(createWordingBox(writer));
        });
    }

    refresh() {
        const model = this.editor.model;
        const selection = model.document.selection;
        const allowedIn = model.schema.findAllowedParent(selection.getFirstPosition(), 'wordingbox');

        this.isEnabled = allowedIn !== null;
    }
}

function createWordingBox(writer) {
    const wordingbox = writer.createElement('wordingbox');
    //const headerBoxTitle = writer.createElement('headerBoxTitle');
    const wording = writer.createElement('wording');

    //writer.append(headerBoxTitle, wordingbox);
    writer.append(wording, wordingbox);

    // There must be at least one paragraph for the description to be editable.
    // See https://github.com/ckeditor/ckeditor5/issues/1464.
    writer.appendElement('paragraph', wording);

    return wordingbox;
}
