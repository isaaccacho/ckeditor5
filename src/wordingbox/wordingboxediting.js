import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import {
    toWidget,
    toWidgetEditable
} from '@ckeditor/ckeditor5-widget/src/utils';
import Widget from '@ckeditor/ckeditor5-widget/src/widget';
import InsertWordingBoxCommand from './insertwordingboxcommand';

export default class WordingBoxEditing extends Plugin {

    static get requires() {
        return [Widget];
    }

    init() {
        console.log('WordingBoxEditing#init() got called');

        this._defineSchema();
        this._defineConverters();

        this.editor.commands.add('insertWordingBox', new InsertWordingBoxCommand(this.editor));
    }

    _defineSchema() {
        //  <wordingbox>
        //    <wording></wording>
        //  </wordingbox>
        const schema = this.editor.model.schema;

        schema.register('wordingbox', {
            isObject: true,
            allowWhere: '$block'
        });

        schema.register('wording', {
            // Cannot be split or left by the caret.
            isLimit: true,

            allowIn: 'wordingbox',

            // Allow content which is allowed in the root (e.g. paragraphs).
            allowContentOf: '$root'
        });

        schema.addChildCheck((context, childDefinition) => {
            if (context.endsWith('wording') && childDefinition.name == 'wordingbox') {
                return false;
            }
        });
    }

    _defineConverters() {
        const conversion = this.editor.conversion;

        // <wordingbox> converters
        conversion.for('upcast').elementToElement({
            model: 'wordingbox',
            view: {
                name: 'section',
                classes: 'wording-box'
            }
        });
        conversion.for('dataDowncast').elementToElement({
            model: 'wordingbox',
            view: {
                name: 'section',
                classes: 'wording-box'
            }
        });
        conversion.for('editingDowncast').elementToElement({
            model: 'wordingbox',
            view: (modelElement, {
                writer: viewWriter
            }) => {
                const section = viewWriter.createContainerElement('section', {
                    class: 'wording-box'
                });

                return toWidget(section, viewWriter, {
                    label: 'wording box widget'
                });
            }
        });

        // <wording> converters
        conversion.for('upcast').elementToElement({
            model: 'wording',
            view: {
                name: 'div',
                classes: 'consent'
            }
        });
        conversion.for('dataDowncast').elementToElement({
            model: 'wording',
            view: {
                name: 'div',
                classes: 'consent'
            }
        });
        conversion.for('editingDowncast').elementToElement({
            model: 'wording',
            view: (modelElement, {
                writer: viewWriter
            }) => {
                // Note: You use a more specialized createEditableElement() method here.
                const div = viewWriter.createEditableElement('div', {
                    class: 'consent'
                });

                return toWidgetEditable(div, viewWriter);
            }
        });
    }
}
