import Plugin from '@ckeditor/ckeditor5-core/src/plugin';
import {
    toWidget,
    toWidgetEditable
} from '@ckeditor/ckeditor5-widget/src/utils';
import Widget from '@ckeditor/ckeditor5-widget/src/widget';
import InsertHeaderBoxCommand from './insertheaderboxcommand';

export default class HeaderBoxEditing extends Plugin {

    static get requires() {
        return [Widget];
    }

    init() {
        console.log('HeaderBoxEditing#init() got called');

        this._defineSchema();
        this._defineConverters();

        this.editor.commands.add('insertHeaderBox', new InsertHeaderBoxCommand(this.editor));
    }

    _defineSchema() {

        //  <headerbox>
        //    <header></header>
        //  </headerbox>

        const schema = this.editor.model.schema;

        schema.register('headerbox', {
            // Behaves like a self-contained object (e.g. an image).
            isObject: true,

            // Allow in places where other blocks are allowed (e.g. directly in the root).
            allowWhere: '$block'
        });

        schema.register('header', {
            // Cannot be split or left by the caret.
            isLimit: true,

            allowIn: 'headerbox',

            // Allow content which is allowed in blocks (i.e. text with attributes).
            allowContentOf: '$block'
        });

        schema.addChildCheck((context, childDefinition) => {
            if (context.endsWith('header') && childDefinition.name == 'headerbox') {
                return false;
            }
        });
    }

    _defineConverters() {
        const conversion = this.editor.conversion;

        // <headerbox> converters
        conversion.for('upcast').elementToElement({
            model: 'headerbox',
            view: {
                name: 'section',
                classes: 'header-box'
            }
        });
        conversion.for('dataDowncast').elementToElement({
            model: 'headerbox',
            view: {
                name: 'section',
                classes: 'header-box'
            }
        });
        conversion.for('editingDowncast').elementToElement({
            model: 'headerbox',
            view: (modelElement, {
                writer: viewWriter
            }) => {
                const section = viewWriter.createContainerElement('section', {
                    class: 'header-box'
                });

                return toWidget(section, viewWriter, {
                    label: 'header box widget'
                });
            }
        });

        // <header> converters
        conversion.for('upcast').elementToElement({
            model: 'header',
            view: {
                name: 'h1',
                classes: 'consentHeader'
            }
        });
        conversion.for('dataDowncast').elementToElement({
            model: 'header',
            view: {
                name: 'h1',
                classes: 'consentHeader'
            }
        });
        conversion.for('editingDowncast').elementToElement({
            model: 'header',
            view: (modelElement, {
                writer: viewWriter
            }) => {
                // Note: You use a more specialized createEditableElement() method here.
                const h1 = viewWriter.createEditableElement('h1', {
                    class: 'consentHeader'
                });

                return toWidgetEditable(h1, viewWriter);
            }
        });
    }
}
