import Command from '@ckeditor/ckeditor5-core/src/command';

export default class InsertHeaderBoxCommand extends Command {
    execute() {
        this.editor.model.change(writer => {
            // Insert <headerbox>*</headerbox> at the current selection position
            // in a way that will result in creating a valid model structure.
            this.editor.model.insertContent(createHeaderBox(writer));
        });
    }

    refresh() {
        const model = this.editor.model;
        const selection = model.document.selection;
        const allowedIn = model.schema.findAllowedParent(selection.getFirstPosition(), 'headerbox');

        this.isEnabled = allowedIn !== null;
    }
}

function createHeaderBox(writer) {
    const headerbox = writer.createElement('headerbox');
    const header = writer.createElement('header');

    writer.append(header, headerbox);

    return headerbox;
}
