import HeaderBoxEditing from './headerboxediting';
import HeaderBoxUI from './headerboxui';
import Plugin from '@ckeditor/ckeditor5-core/src/plugin';

export default class HeaderBox extends Plugin {
    static get requires() {
        return [HeaderBoxEditing, HeaderBoxUI];
    }
}